import NukeBull 
import EncrypText

import System.Environment
import Data.Char
import Data.Word
import Data.Maybe

import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as TIO

import qualified Data.ByteString as B
import qualified Data.ByteString.Base64 as BS


main = do 
       (comando:_) <- getArgs

       putStrLn ""
       putStrLn "================================================================="
       putStrLn "=====   N   N  U  U  K  K  EEEE     BBB   U  U  L    L      ====="
       putStrLn "=====   NN  N  U  U  K K   E        B  B  U  U  L    L      ====="
       putStrLn "=====   N N N  U  U  KK    EEE      BBB   U  U  L    L      ====="
       putStrLn "=====   N  NN  U  U  K K   E        B  B  U  U  L    L      ====="
       putStrLn "=====   N   N  UUUU  K  K  EEEE     BBB   UUUU  LLLL LLLL   ====="
       putStrLn "================================================================="
       putStrLn "                         HackMadrid%27"
       putStrLn "================================================================="
       putStrLn ""


       case comando of 

        "encrypt" -> do 
                     (_:fileName10:_) <- getArgs
                     let fileName1 = "Keys/" ++ fileName10 ++ ".key"                     

                     -- Read Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 Cipher Lv 3.2
                     source <- B.readFile fileName1

                     -- Text to crypt
                     putStrLn "================================================================="
                     putStrLn "Texto para Cifrar (Número máximo de caracteres: 127) (No usar ASCII 217 ┘)"                    
                     putStrLn "================================================================="

                     textoOn <- TIO.getLine 
                     if T.length (textoOn) > 127 
                        then putStrLn $ " Yours Characters are: " ++ (show(T.length (textoOn))) ++" (Number Maximun of characters are 127) "
                        else do
                           texto <- encryptext (E.encodeUtf8 textoOn) source
                           let base64 = BS.encode texto
                           putStrLn "================================================================="
                           putStrLn $ show(base64)
                           putStrLn "================================================================="


                    
        "decrypt" -> do 
                     (_:fileName10:_) <- getArgs
                     let fileName1 = "Keys/" ++ fileName10 ++ ".key"

                     -- Read Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 RandomGen Lv 3.2
                     source <- B.readFile fileName1
                     
                     -- Introduce el codigo a descifrar
                     putStrLn "================================================================="
                     putStrLn "Introduce El Codigo para Descifrarlo"
                     putStrLn "================================================================="
                     code <- TIO.getLine
                     let ebase64 = BS.decode (E.encodeUtf8 code)      
                     
                     case ebase64 of
                      Left (base64) -> putStrLn "No esta completo/correcto el codigo quehas introducido"
                      Right (base64)-> do

                       if  B.length base64 == 256   
                        then do 
                            text <- decryptext base64 source
                            -- The 217 ASCII is the end of the text
                            let textoInt =  B.elemIndex 217 text
                            --TIO.putStrLn  (E.decodeUtf8(B.take (fromJust textoInt) text))
                            
                            putStrLn "================================================================="
                            TIO.putStrLn  (E.decodeUtf8(B.take (fromJust textoInt) text))
                            putStrLn "================================================================="


                        else do
                            -- No More
                            putStrLn "Is it a Cryptext File?"                    

        "info" -> do
            putStrLn "================================================================="
            putStrLn "                    Como ejecutar CrypText"
            putStrLn "================================================================="
            putStrLn ""
            putStrLn "       Cifrar: runhaskell CrypText.hs encrypt <password_name>"
            putStrLn "     Descifrar: runhaskell CrypText.hs decrypt <password_name>"
            putStrLn "          Informacion: runhaskell CrypText.hs info"
            putStrLn ""
            putStrLn "================================================================="




        otherwise ->do
            putStrLn "================================================================="
            putStrLn "       ¿No te habras confundido al escribir los argumentos?"
            putStrLn "================================================================="
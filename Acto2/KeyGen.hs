import NukeBull (randomB,noOrder)
import System.Environment
import Data.Word
import Data.Bits
import System.Random

import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as TIO


import qualified Data.ByteString as B
import qualified Data.ByteString.Base64 as BS



generator :: (RandomGen g ) => Int -> Int -> g ->  [Int]
generator n m g = take n $ randomRs (0,m) g

choose :: Int -> B.ByteString -> B.ByteString
choose n b = B.drop (n-1) (B.take n b )

ordercrazy :: Int -> [Word8] ->[Int] -> [Int] -> [Word8]
ordercrazy 1 c s1 s2 = noOrder c s1 s2
ordercrazy n c s1 s2 = noOrder ( ordercrazy (n-1) c s1 s2 )s1 s2 

crazyloop :: (RandomGen g) => Int -> Int -> g -> IO B.ByteString
crazyloop n m g = do
          poll <- randomB m
          g <- newStdGen
          let droping = generator n (m-1) g
          let password = B.concat ( map (\x -> B.drop x (B.take (x+1) poll)) droping )
          return password




main = do 
    (comando:_) <- getArgs

    putStrLn ""
    putStrLn "================================================================="
    putStrLn "=====   N   N  U  U  K  K  EEEE     BBB   U  U  L    L      ====="
    putStrLn "=====   NN  N  U  U  K K   E        B  B  U  U  L    L      ====="
    putStrLn "=====   N N N  U  U  KK    EEE      BBB   U  U  L    L      ====="
    putStrLn "=====   N  NN  U  U  K K   E        B  B  U  U  L    L      ====="
    putStrLn "=====   N   N  UUUU  K  K  EEEE     BBB   UUUU  LLLL LLLL   ====="
    putStrLn "================================================================="
    putStrLn "                         HackMadrid%27"
    putStrLn "================================================================="
    putStrLn ""



    case comando of 

        "gen" -> do   
                (_:fileName10:_) <- getArgs                  
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                -- Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 RandomGen Lv 3.2              

                password <- randomB 128                  
                B.writeFile fileName1 password                    
                putStrLn "Creando  vectorIV -------------  OK"
                putStrLn "Creando  AES256 Key -----------  OK"
                putStrLn "Creando  Password Lv. 3.1 -----  OK"
                putStrLn "Creando  Password Lv. 3.2 -----  OK"
                putStrLn "Creando  Password -------------  OK"
                let base64 = BS.encode password
                putStrLn "================================================================="
                putStrLn $ show(base64)
                putStrLn "================================================================="
                

        "write" -> do   
                (_:fileName10:_) <- getArgs               
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                -- Introduce el codigo de la contraseña
                putStrLn "================================================================="
                putStrLn "                Introduce El codigo de la contraseña             "
                putStrLn "================================================================="
                code <- TIO.getLine
                putStrLn "================================================================="
                let base64 = BS.decode (E.encodeUtf8 code)
                case base64 of
                        Left (password) -> putStrLn "No esta completo/correcto el codigo que has introducido"
                        Right (password)-> do
                                  B.writeFile fileName1 password
                                  putStrLn "Escribiendo  vectorIV -------------  OK"
                                  putStrLn "Escribiendo  AES256 Key -----------  OK"
                                  putStrLn "Escribiendo  Password Lv. 3.1 -----  OK"
                                  putStrLn "Escribiendo  Password Lv. 3.2 -----  OK"
                                  putStrLn "Escribiendo  Password -------------  OK"

        "read" -> do 
                (_:fileName10:_) <- getArgs               
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                password <- B.readFile fileName1
                let base64 = BS.encode password
                putStrLn "================================================================="
                putStrLn $ show(base64)
                putStrLn "================================================================="
                

        
        "crazy" -> do   
                        (_:fileName10:_) <- getArgs                  
                        let fileName1a = "Keys/" ++ fileName10 ++ "a.key"
                        let fileName1b = "Keys/" ++ fileName10 ++ "b.key"
                        let fileName1 = "Keys/" ++ fileName10 ++ ".key"
        
                        -- Read Password = 128 bytes = 16 vectorIV + 32 AES256 + 16  Cipher Lv.3.1 + 64 RandomGen Lv 3.2              
                        
                        putStrLn "Creating Random Poll ---------------------------  OK"
                        putStrLn "Please wait, Millions of bits Playing ----------  OK"              
        
                        g <- getStdGen 
        
                        password1 <- crazyloop 64 1048576 g
                        putStr "[------25%"
                        g <- newStdGen                
                        password2 <- crazyloop 64 1048576 g
                        putStr "------50%"
                        g <- newStdGen                
                        password3 <- crazyloop 64 1048576 g
                        putStr "------75%"
                        g <- newStdGen                
                        password4 <- crazyloop 64 1048576 g
                        putStr "------100%]"
                        let passwords = B.unpack(password1) ++ B.unpack(password2) ++ B.unpack(password3) ++ B.unpack(password4)
                        putStrLn ""
                        g <- newStdGen
                        let rows = generator 32 15 g
                        g <- newStdGen
                        let cols = generator 32 15 g
        
                        let password = B.take 128 (B.pack (ordercrazy 30 passwords rows cols))
                        let passworda = B.drop 128 (B.pack (ordercrazy 30 passwords rows cols))
                        let passwordb = B.pack (B.zipWith (xor) ( password) ( passworda))
                                                      
                        B.writeFile fileName1 password
        

                        putStrLn "================================================================="
                        putStrLn "   Esta es tu clave, generada con millones de bits aleatorios    "
                        putStrLn "================================================================="
                        putStrLn "================================================================="
                        putStrLn $ show(BS.encode password)
                        putStrLn "================================================================="
        
                        let base64a = BS.encode passworda
                        let base64b = BS.encode passwordb
        
                        putStrLn ""
                        putStrLn "================================================================="
                        putStrLn "A partir de los siguientes códigos podras volver a crear la clave"
                        putStrLn "           Para unirla runhaskell xor <passwoed_name>            "
                        putStrLn "================================================================="
                        putStrLn $ show(BS.encode passworda)
                        putStrLn "================================================================="
                        putStrLn "================================================================="
                        putStrLn $ show(BS.encode passwordb)
                        putStrLn "================================================================="
                        B.writeFile fileName1a passworda
                        B.writeFile fileName1b passwordb
        
                        putStrLn  "================================================================"
                        putStrLn ("     Your password has been created in " ++ (show(fileName1)))
                        putStrLn (" Two Complementary keys in " ++ (show(fileName1a)) ++" and "++ (show(fileName1b)))
                        putStrLn  "================================================================"
                        putStrLn ""
        
        "xor" -> do
                --(_:fileName10:_) <- getArgs
                -- let fileName1 = "Keys/" ++ fileName10 ++ ".key"
                --let fileName1a = "Keys/" ++ fileName10 ++ "a.key"
                --let fileName1b = "Keys/" ++ fileName10 ++ "b.key"
                (_:fileName10:_) <- getArgs               
                let fileName1 = "Keys/" ++ fileName10 ++ ".key"

                -- Introduce el codigo de la contraseña 1
                putStrLn "================================================================="
                putStrLn "         Introduce El codigo de la primera contraseña            "
                putStrLn "================================================================="
                code1 <- TIO.getLine
                putStrLn "================================================================="
                let key1 = BS.decode (E.encodeUtf8 code1)
                case key1 of
                        Left (password1) -> putStrLn "No esta completo/correcto el codigo que has introducido"
                        Right (password1)-> do
                                              -- Introduce el codigo de la contraseña 2
                           putStrLn "================================================================="
                           putStrLn "          Introduce El codigo de la segunda contraseña           "
                           putStrLn "================================================================="
                           code2 <- TIO.getLine
                           putStrLn "================================================================="
                           let key2 = BS.decode (E.encodeUtf8 code2)
                           case key2 of
                                Left (password2) -> putStrLn "No esta completo/correcto el codigo que has introducido"
                                Right (password2)-> do
                                        let password = B.pack (B.zipWith (xor) ( password1) (password2))
                                        B.writeFile fileName1 password
                                        putStrLn "================================================================="
                                        putStrLn "                   Nueva contraseña generada                     "
                                        putStrLn "================================================================="

                                        putStrLn ""
                                        putStrLn "================================================================="
                                        putStrLn $ show(BS.encode password)
                                        putStrLn "================================================================="

                                        putStrLn ""
                                        putStrLn ""
                                        putStrLn ("     Your password has been created in " ++ (show(fileName1)))
                                        putStrLn ""    

        "info" -> do
                putStrLn "================================================================="
                putStrLn "                    Como usar KeyGen.hs"
                putStrLn "================================================================="
                putStrLn ""
                putStrLn "Nueva Clave Normal: runhaskell KeyGen.hs gen <password_name>"
                putStrLn "Nueva Clave Crazy : runhaskell KeyGen.hs crazy <password_name>"              
                putStrLn "Mostar Clave      : runhaskell KeyGen.hs read <password_name>"
                putStrLn "Escribir Clave    : runhaskell KeyGen.hs write <password_name>"
                putStrLn "Unir dos Claves   : runhaskell KeyGen.hs xor <password_name>"
                putStrLn "Información       : runhaskell KeyGen.hs info"
                putStrLn ""
                putStrLn "================================================================="
                                    
                                       
        otherwise ->do
            putStrLn "================================================================="
            putStrLn "       ¿No te habras confundido al escribir los argumentos?"
            putStrLn "================================================================="

          

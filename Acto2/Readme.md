# Ejecutar con runhaskell.

## Obtener información.
~~~
runhaskell KeyGen.hs info
runhaskell CrypText.hs info
~~~

# Crear contraseña.

## Nueva.
### runhaskell KeyGen.hs gen <password_name>
~~~
runhaskell KeyGen.hs gen Password
~~~

## Nueva (Paranoic@).
### runhaskell KeyGen.hs crazy <password_name>
~~~
runhaskell KeyGen.hs crazy Password
~~~

## Mostrar codigo de una contraseña.

### runhaskell KeyGen.hs read <password_name>
~~~
runhaskell KeyGen.hs read Password
~~~
## Generar contraseña mediante un código.

### runhaskell KeyGen.hs write <password_name>
~~~
runhaskell KeyGen.hs write Password
~~~

## Generar contraseña con dos códigos.

### runhaskell KeyGen.hs xor <password_name>
~~~
runhaskell KeyGen.hs xor Password
~~~

# Crear texto Cifrado.

### runhaskell CrypText.hs encrypt <password_name>
~~~
runhaskell CrypText.hs encrypt Password
~~~

# Leer texto Cifrado.

### runhaskell CrypText.hs decrypt <password_name>
~~~
runhaskell CrypText.hs decrypt Password
~~~

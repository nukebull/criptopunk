### Para la instalación del contenedor Docker, publicamos el Dockerfile en formato texto para poder auditar que es lo que instalamos.

### Utilizamos Arch Linux Base para Docker (versión Docker de la distribución Arch Linux) y sobre ésta, desde el repositorio oficial de Arch Linux los paquetes necesarios para usar Haskell, el paquete git para poder bajar el material de la charla, el editor nano para poder editar archivos, gpg para cifrado simetrico-asimetrico y steghide para mensajes incrustados (Esteganografía).

### El repositorio git no contiene ningun archivo compilado, todo esta en .txt .md .hs (haskell code). Aparte tendrá una imágen .jpg para probar steghide y eBook sobre docker en formato pdf como ayuda. https://framagit.org/nukebull/criptopunk

### Se procede de esta manera, para poder auditar el software antes de su descarga.
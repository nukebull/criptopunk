# Suponemos que tenemos instalado docker en nuestro equipo, cada sistema operativo debe seguir su propio procedimiento.

## 0.a. Si no queremos usar root como usuario docker.
#### sudo usermod -aG docker ${USER}
~~~
sudo usermod -aG docker nukebull
~~~

### ¡¡Importante!! Debemos Reiniciar el S.O.

## 0.b. Probamos que Docker esta corriendo.
~~~
docker info
~~~

## 0.c. Si no corre debemos iniciar el servicio.
~~~
sudo systemctl start docker.service
~~~

## 0.d. Probamos si ya esta todo bien.
~~~
systemctl status docker.service
~~~

## 0.d. Si queremos que arranque el servicio con el PC.
~~~
systemctl enable docker.service
~~~

## 1. Construir Imagen para correr haskell con Arch Linux (en el directorio que tengamos Dockerfile).
# ¡¡Impotarte!! no olvidar el . al final

### docker build -t <image_name> .
~~~
docker build -t nukebull/criptopunk .
~~~

## 2. Miramos IMAGE ID con el siguiente comando para crear el contenedor
~~~
docker images 
~~~

***

# Sin usar Volumenes.

## 3.a. Creamos el contenedor, usaremos el nombre del campo REPOSITORY que nos muestra 'docker images' para <image_name>.
#### docker run -it --name <container_name> <image_name>
~~~
docker run -it --name criptopunk nukebull/criptopunk
~~~

## 4.a Cuando estemos dentro del contenedor /home descargamos la charla.
~~~
git clone https://framagit.org/nukebull/criptopunk.git
~~~
***

# Usando Volumenes.

## 3.b. Si queremos crear un volumen.
#### docker volume create --name <volume_name>
~~~
docker volume create --name criptopunk
~~~

## 4.b. Comprobamos donde se gusra el volumen y si existe.
#### docker volume inspect <volume_name>
~~~
docker volume inspect criptopunk
~~~

## 5.b. Creamos y lanzamos el contenedor, usaremos el nombre del campo REPOSITORY que nos muestra 'docker images' para <image_name>.
#### docker run -it --name <container_name>  -v <volume_name>:<punto_montaje> <image_name>
~~~
docker run -it --name criptopunk  -v criptopunk:/home nukebull/criptopunk
~~~

## 6.b Cuando estemos dentro del contenedor /home descargamos la charla.
~~~
git clone https://framagit.org/nukebull/criptopunk.git
~~~
***


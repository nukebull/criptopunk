# Como probar el código.

## EncryptDark.hs  y DecryptDark.hs
~~~
runhaskell EncryptDark.hs
runhaskell DecryptDark.hs
~~~

## EncryptDark2.hs, DecryptDark2.hs y DecryptDarker2.hs
~~~
runhaskell EncryptDark2.hs
runhaskell DecryptDark2.hs
runhaskell DecryptDarker2.hs
~~~

## Números aleatorios.
~~~
runhaskell Random.hs
~~~
import System.Random
import Data.Word

import qualified Crypto.Random.Entropy as E

import qualified Data.ByteString as B

--  Nunbers for Cryptography

randomB :: Int -> IO B.ByteString
randomB n = do
    code <- E.getEntropy n
    return code




--- "BAD" Nunbers for Cryptography only random

-- Randon generator, Int List Numbers (0-255)
-- Need Random g for init (g <- getStdGen, g <- newStdGen)

generator256 :: (RandomGen g ) => Int -> g ->  [Int]
generator256 n g = take n $ randomRs (0,255) g

-- Gen n random Word8 
-- Need Random g for init (g <- getStdGen, g <- newStdGen)

randomBytes :: (RandomGen g) => Int -> g -> [Word8]
randomBytes 0 _ = []
randomBytes n g = map fromIntegral (generator256 n g)

-- Gen Random ByteString n 
-- Need Random g for init (g <- getStdGen, g <- newStdGen)

randomBytesString :: (RandomGen g) => Int -> g -> B.ByteString
randomBytesString n g = B.pack $ randomBytes n g




main = do
    putStrLn ""
    putStrLn "Usando Entropy-CryptographyRandomNumbers (buena implementación)"
    putStrLn ""

    code <- randomB 32   
    putStrLn $ show(B.unpack code)

    putStrLn ""
    putStrLn "Generamos de nuevo otra serie de números aleatorios"
    putStrLn ""

    code2 <- randomB 32
    putStrLn $ show(B.unpack code2)

    putStrLn ""
    putStrLn "Usando System.Random (Aleatorios para estadistica)"
    putStrLn ""
    putStrLn "Cuidado con olvidar sembrar nuestras semillas"
    putStrLn ""

    g <- getStdGen
    g <- newStdGen
    
    let numbers = randomBytesString 32 g
    putStrLn $ show (B.unpack numbers)

    putStrLn ""
    putStrLn "No generamos una nueva semilla, un pequeño olvido"
    putStrLn ""

    let numbers2 = randomBytesString 32 g
    putStrLn $ show (B.unpack numbers2)

    g <- newStdGen

    putStrLn ""
    putStrLn "Ahora si tenemos una nueva semilla"
    putStrLn ""

    let numbers2 = randomBytesString 32 g
    putStrLn $ show (B.unpack numbers2)






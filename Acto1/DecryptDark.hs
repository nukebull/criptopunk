import Data.Word
import Data.Char
import qualified Data.ByteString as B
import qualified Crypto.Random.Entropy as E
import qualified Data.ByteString.Char8 as BC
import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (BlockCipher(..), Cipher(..),IV, makeIV)
import Crypto.Error (throwCryptoError)
import qualified Data.Text as T
import qualified Data.Text.Encoding as EN
import qualified Data.Text.IO as TIO
import Crypto.KDF.Scrypt (generate, Parameters(..))
import Crypto.Hash


-- Parameters Coling Parcival's key derivation function

paramN = 32 :: Word64
paramR = 8
paramP = 1
paramKeyLen = 32

--Scrypt KDF - Coling Parcival's key derivation function
deriveKey :: B.ByteString -> B.ByteString -> B.ByteString
deriveKey password salt = generate params  password salt
    where params = Parameters {n = paramN, r = paramR, p = paramP, outputLength = paramKeyLen}


decryptAES256 :: B.ByteString -> IV AES256 -> B.ByteString  -> B.ByteString
decryptAES256 key cIV plainData = cbcDecrypt ctx cIV plainData
        where ctx :: AES256
              ctx = throwCryptoError $ cipherInit key 


-- Crea un String haciendo hash al ByteString de entrada
hashing :: B.ByteString -> String
hashing b = show (hashWith SHA256 b)

main = do 


    -- Cargamos el fichero 
    source <- B.readFile "Secret.crypt"
 
    let vectorIV = B.pack (take 16 (B.unpack source ))
    let textocrypt = B.pack ( drop 16 $ B.unpack source )

    putStrLn ""
    putStrLn "Generamos AES256-key a partir de un texto" 
    putStrLn "La contraseña ALTERADA. Fue un error de programacíon, nunca ha sido nuestra intención. "
    putStrLn ""
    putStrLn "-----------------------------------"
    putStrLn " Introduce la palabra o contraseña "
    keyOn <- TIO.getLine  
    putStrLn "-----------------------------------"

    -- DARK 
    -- Nos quedamos los 2 primeros caracteres de la clave (B.take 2)     
    let keyString = hashing (EN.encodeUtf8  keyOn)
    putStrLn $ "El Hash de la clave es" ++ show(keyString)
    putStrLn ""
    --Nos quedamos los 6 primeros caracteres del hash de la clave (B.take 6)
    --Desmarca la siguiente linea para añadir hash key 
    --let keyString2 = B.take 6 ( BC.pack (keyString))
    --Comenta la siguiente linea si has desmarcado la anterior
    let keyString2 = B.take 2 $ EN.encodeUtf8  keyOn
    let keySaltString = BC.pack "salt"
    let key2 = deriveKey keyString2 keySaltString

    
    let mInitIV = makeIV vectorIV
    case mInitIV of
            Nothing -> error "Failed to generate and initialization vector."
            Just initIV -> do
                 let dtexto = decryptAES256 key2 initIV textocrypt
                 TIO.putStrLn  (EN.decodeUtf8(dtexto))
                 --putStrLn $ show (dtexto)



     
                  

import Data.Word
import Data.Char
import System.Environment
import qualified Data.ByteString as B
import qualified Crypto.Random.Entropy as E
import qualified Data.ByteString.Char8 as BC
import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (BlockCipher(..), Cipher(..),IV, makeIV)
import Crypto.Error (throwCryptoError)
import qualified Data.Text as T
import qualified Data.Text.Encoding as EN
import qualified Data.Text.IO as TIO
import Crypto.KDF.Scrypt (generate, Parameters(..))
import Crypto.Hash

-- Parameters Coling Parcival's key derivation function

paramN = 32 :: Word64
paramR = 8
paramP = 1
paramKeyLen = 32

--Scrypt KDF - Coling Parcival's key derivation function
deriveKey :: B.ByteString -> B.ByteString -> B.ByteString
deriveKey password salt = generate params  password salt
    where params = Parameters {n = paramN, r = paramR, p = paramP, outputLength = paramKeyLen}

-- Generador de Números
randomBS :: Int -> IO B.ByteString
randomBS n = do
    code <- E.getEntropy n
    return code

-- Extensión del texto para hacerlo multiplo de 16 bytes
extender :: B.ByteString -> Int -> B.ByteString
extender b 0 = b
extender b n = B.pack (B.unpack(extender b (n-1)) ++ [32])    

-- AES256
encryptAES256 ::  B.ByteString -> IV AES256 -> B.ByteString  -> B.ByteString
encryptAES256 key cIV plainData = cbcEncrypt ctx cIV plainData
        where ctx :: AES256
              ctx = throwCryptoError $ cipherInit key    

-- Crea un String haciendo hash al ByteString de entrada
hashing :: B.ByteString -> String
hashing b = show (hashWith SHA256 b)


main = do 

    -- Cargamos el fichero 
    --key <- B.readFile filename
    source2 <- B.readFile "Secret.txt"

    -- Extendemos el texto con espacios en blanco.
    let longitud = B.length source2
    let div16 = div longitud  16
    let resto16 = mod longitud  16
    let unrest16 = 16 - resto16
    let source = extender source2 unrest16


    putStrLn "Este programa es para uso educativo"
    putStrLn "Generamos AES256-key a partir de un texto" 
    putStrLn "La contraseña ALTERADA. Fue un error de programacíon, nunca ha sido nuestra intención. "
    putStrLn ""


    putStrLn "---------------------------------"
    putStrLn "Introduce la palabra o contraseña"
    keyOn <- TIO.getLine  
    putStrLn "---------------------------------"

    -- debiamos haber hecho esto
    --let keyString = EN.encodeUtf8  keyOn
    --let key = deriveKey keyString keySaltString    
    -- DARK 
    -- Nos quedamos los 2 primeros caracteres de la clave (B.take 2)
     
    let keyString = hashing (EN.encodeUtf8  keyOn)
    putStrLn $ "El Hash de la clave es" ++ show(keyString)
    --Nos quedamos los 6 primeros caracteres del hash de la clave (B.take 6)
    --Desmarca la siguiente linea para añadir hash key 
    --let keyString2 = B.take 6 ( BC.pack (keyString))
    --Comenta la siguiente linea si has desmarcado la anterior
    let keyString2 = B.take 2 $ EN.encodeUtf8  keyOn
    let keySaltString = BC.pack "salt"
    let key2 = deriveKey keyString2 keySaltString
    
    -- vector IV AES256 128 bites = 16 bytes
    vectorIV <- randomBS 16
    --putStrLn $ show (B.unpack vectorIV)
    

    let mInitIV = makeIV vectorIV
    case mInitIV of
            Nothing -> error "Failed to generate and initialization vector."
            Just initIV -> do
                let texto = encryptAES256 key2 initIV source
                let textcrypt = B.pack( (B.unpack vectorIV) ++ (B.unpack texto))
                putStrLn "-----------------------------------------------------------"
                putStrLn " Archivo creado (vectorIV + Texto Cifrado) en Secret.crypt "
                putStrLn "-----------------------------------------------------------"
                --putStrLn $ show (B.unpack textcrypt)
                B.writeFile "Secret.crypt" textcrypt 
                  

import Data.Word
import Data.Char
import qualified Data.ByteString as B
import qualified Crypto.Random.Entropy as E
import qualified Data.ByteString.Char8 as BC
import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (BlockCipher(..), Cipher(..),IV, makeIV)
import Crypto.Error (throwCryptoError)
import qualified Data.Text as T
import qualified Data.Text.Encoding as EN
import qualified Data.Text.IO as TIO
import Crypto.KDF.Scrypt (generate, Parameters(..))



-- Parameters Coling Parcival's key derivation function

paramN = 32 :: Word64
paramR = 8
paramP = 1
paramKeyLen = 32

--Scrypt KDF - Coling Parcival's key derivation function
deriveKey :: B.ByteString -> B.ByteString -> B.ByteString
deriveKey password salt = generate params  password salt
    where params = Parameters {n = paramN, r = paramR, p = paramP, outputLength = paramKeyLen}


decryptAES256 :: B.ByteString -> IV AES256 -> B.ByteString  -> B.ByteString
decryptAES256 key cIV plainData = cbcDecrypt ctx cIV plainData
        where ctx :: AES256
              ctx = throwCryptoError $ cipherInit key 


main = do 


    -- Cargamos el fichero 
    source <- B.readFile "Secret2.crypt"
    let textLength = B.length source

 
    let vectorIV = B.pack (take 16 (B.unpack source ))
    let textocrypt = B.pack (take (textLength-48) (drop 16 $ B.unpack source ))

    putStrLn ""
    putStrLn "-------------------------------------------------------------"
    putStrLn "No confíes en todo lo que ves,"
    putStrLn "no creas en todo lo que escuchas y no digas todo lo que sabes"
    putStrLn "-------------------------------------------------------------"
    putStrLn ""
    putStrLn "-----------------------------------"
    putStrLn " Introduce la palabra o contraseña "
    keyOn <- TIO.getLine  
    putStrLn "-----------------------------------"

    -- Extendemos la clave-password
    let keyString = EN.encodeUtf8  keyOn
    let keySaltString = BC.pack "salt"
    let key = deriveKey keyString keySaltString

    
    let mInitIV = makeIV vectorIV
    case mInitIV of
            Nothing -> error "Failed to generate and initialization vector."
            Just initIV -> do
                 let dtexto = decryptAES256 key initIV textocrypt
                 TIO.putStrLn  (EN.decodeUtf8(dtexto))
                 --putStrLn $ show (dtexto)
                 --B.writeFile "Secret.txt" dtexto  



     
                  

import Data.Word
import Data.Char
import qualified Data.ByteString as B
import qualified Crypto.Random.Entropy as E
import qualified Data.ByteString.Char8 as BC
import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types (BlockCipher(..), Cipher(..),IV, makeIV)
import Crypto.Error (throwCryptoError)
import qualified Data.Text as T
import qualified Data.Text.Encoding as EN
import qualified Data.Text.IO as TIO





decryptAES256 :: B.ByteString -> IV AES256 -> B.ByteString  -> B.ByteString
decryptAES256 key cIV plainData = cbcDecrypt ctx cIV plainData
        where ctx :: AES256
              ctx = throwCryptoError $ cipherInit key 


main = do 


    -- Cargamos el fichero 
    source <- B.readFile "Secret2.crypt"
    let textLength = B.length source
    
    -- Estraemos clave, vector IV y texto cifrado
    let key = B.pack (drop (textLength-32) $ B.unpack source )
    let vectorIV = B.pack (take 16 (B.unpack source ))
    let textocrypt = B.pack (take (textLength-48) (drop 16 $ B.unpack source ))

    putStrLn ""
    putStrLn "-------------------------------------------------------------"
    putStrLn "No confíes en todo lo que ves,"
    putStrLn "No creas en todo lo que escuchas y no digas todo lo que sabes"
    putStrLn "-------------------------------------------------------------"
    putStrLn ""
    putStrLn "---------------------------------"
    putStrLn "Imaginate que que tu clave no fuese tan secreta"
    putStrLn "¿Para que pedirtela? si la conozco ya."
    putStrLn "---------------------------------"
    putStrLn ""


    
    let mInitIV = makeIV vectorIV
    case mInitIV of
            Nothing -> error "Failed to generate and initialization vector."
            Just initIV -> do
                 let dtexto = decryptAES256 key initIV textocrypt
                 TIO.putStrLn  (EN.decodeUtf8(dtexto))
                 --putStrLn $ show (dtexto)
                 --B.writeFile "Secret.txt" dtexto  



     
                  

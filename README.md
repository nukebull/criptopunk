# CriptoPunk - Criptografía.

### Trataremos los siguientes temas.

1. ####  La importancia del sofware libre en la criptografía haciendo hincapié en la necesidad de auditar el codigo que protegerá nuestros datos.

2. #### AES-Hardening. Cifrado de texto sobre Telegram, WhatsApp,...

3. #### Informacion comprometida en medios hostiles. Esteganografía y redes P2P.

#### Aparte se podrá crear un Contenedor Docker para ejecutar los códigos de la charla.

##### Todos los archivos con el código escrito en Haskell están bajo licencia GPL v3.

##### Los archivos de texto o markdown están bajo licencia Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License.

##### El libro sobre Docker esta bajo licencia Creative Commons.
